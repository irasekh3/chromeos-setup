# chromeos-setup

This project is used for me to be able to quickly setup a new chrome os devices linux environment.

run the script by calling:
``` bash
sh -c "$(curl -fsSL https://gitlab.com/irasekh3/chromeos-setup/raw/master/scripts/configure.sh)"
```

# A refresh of the terminal may be required 

# Install Terminator or XFCE by running the following commands:
``` bash                                                                                                             
sh -c "$(curl -fsSL https://gitlab.com/irasekh3/chromeos-setup/raw/master/scripts/linux_software_center_install.sh)"                     
```
# then reboot the device, reopen the terminal and run the following to open up the software app:
``` bash
xhost +si:localuser:root
sudo gnome-software
```
#note these commands must be run everytime you would like to open the linux software center

# Manual Configuration Required
# Run :PlugInstall from neovim
# Open up terminal and setup theme / keymaping and other desired configurations

